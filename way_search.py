# входные данные
n = int(input())
coord_dict = {}
for i in range(n):
    coord_dict[i + 1] = list(map(int, input().split()))
k = int(input())
start, finish = list(map(int, input().split()))
# список со всеми возможными путями
possible_ways = [[start]]
# для проверки наличия решения
check = False
while possible_ways != []:
    temp_dict = []
    for way in range(len(possible_ways)):
        cur = possible_ways[way][-1]
        for key in coord_dict:
            # проверка отдалённости от конечной точки (чтобы не продолжать искать маршрут, когда слишком далеко отдалились
            if abs(coord_dict[finish][0] - coord_dict[key][0]) + abs(coord_dict[finish][1] - coord_dict[key][1]) < k * (n - x):
                # проверка использования этой точки раньше в маршруте
                if key not in possible_ways[way]:
                    length = abs(coord_dict[cur][0] - coord_dict[key][0]) + abs(coord_dict[cur][1] - coord_dict[key][1])
                    # проверка досягаемости
                    if length <= k:
                        temp_dict += [possible_ways[way] + [key]]
                        # если точка - последняя, то можно дальше не искать
                        if key == finish:   
                            check = True
                            minimum = len(temp_dict[-1]) - 1
    possible_ways = temp_dict
    if check:
        possible_ways = []
        print(minimum)
# в случае не нахождения ничего
if not check:
    print(-1)

def func(mas):
    global n
    out_mas = []
    for out in mas:
        if out.count('(') > out.count(')'):
            out_mas += [out + ')']
        if out.count('(') < n:
            out_mas += [out + '(']
    return out_mas
n = int(input())

out = ['']
for _ in range(2 * n):
    out = func(out)
out = sorted(set(out))
for el in out:
    print(el)
